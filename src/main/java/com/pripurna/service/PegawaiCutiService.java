package com.pripurna.service;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.pripurna.dao.impl.PegawaiCutiDaoImpl;
import com.pripurna.dto.PegawaiCutiDto;
import com.pripurna.model.PegawaiCuti;
import com.pripurna.report.excel.ExcelPegawaiCutiReport;
import com.pripurna.report.pdf.PdfPegawaiCutiReport;
import com.pripurna.util.ModelDataTable;
import com.pripurna.util.PagerModel;

@Service
@Transactional
public class PegawaiCutiService {
	
	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 3;
	private static final int[] PAGE_SIZES = { 3, 5, 10, 25, 50, 75, 100 };

	@Autowired
	private PegawaiCutiDaoImpl pegawaiCutiDaoImpl;
	
	public PegawaiCuti save(PegawaiCuti pegawaiCuti) {
		return pegawaiCutiDaoImpl.save(pegawaiCuti);
	}

	public PegawaiCuti edit(PegawaiCuti pegawaiCuti) {
		return pegawaiCutiDaoImpl.save(pegawaiCuti);
	}

	public void delete(Long id) {
		pegawaiCutiDaoImpl.delete(id);
	}

	public PegawaiCuti findById(Long id) {
		return pegawaiCutiDaoImpl.findById(id);
	}
	
	public PegawaiCutiDto dtoFindById(Long id) {
		return pegawaiCutiDaoImpl.dtoFindById(id).get();
	}

	public ModelDataTable<PegawaiCutiDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");
		Page<PegawaiCutiDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = pegawaiCutiDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = pegawaiCutiDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection),
					evalSearch);
		}

		PagerModel pager = new PagerModel(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
		ModelDataTable<PegawaiCutiDto> model = new ModelDataTable<>();
		model.setData(pages);
		model.setEvalPage(evalPage);
		model.setPagerModel(pager);
		model.setPageSize(PAGE_SIZES);
		model.setSelectedPageSize(evalPageSize);
//		model.setTotalRow(bagianDaoImpl.findAll().size());
		model.setResultCount(pages.getSize());
//		model.setAuthName(auth.getAuthentication().getName());

		return model;
	}

	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {

		String type = req.getParameter("type");

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");

		Page<PegawaiCutiDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = pegawaiCutiDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = pegawaiCutiDaoImpl.pagination(
					PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection), evalSearch);
		}

		List<PegawaiCutiDto> lists = pages.getContent();

		int number = pages.getNumber();
		int size = pages.getSize();

		Map<String, Object> mapModel = new HashMap<>();
		mapModel.put("reports", lists);
		mapModel.put("number", number);
		mapModel.put("size", size);

		ExcelPegawaiCutiReport excel = new ExcelPegawaiCutiReport();
		PdfPegawaiCutiReport pdf = new PdfPegawaiCutiReport();
		if (type.equals("xls")) {
			return new ModelAndView(excel, mapModel);
		} else if (type.equals("pdf")) {
			return new ModelAndView(pdf, mapModel);
		}
		return new ModelAndView(new ExcelPegawaiCutiReport(), mapModel);
	}
}
