package com.pripurna.service;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.pripurna.dao.impl.BagianDaoImpl;
import com.pripurna.dto.BagianDto;
import com.pripurna.model.Bagian;
import com.pripurna.report.excel.ExcelBagianReport;
import com.pripurna.report.pdf.PdfBagianReport;
import com.pripurna.util.ModelDataTable;
import com.pripurna.util.PagerModel;

@Service
@Transactional
public class BagianService {

	@Autowired
	private BagianDaoImpl bagianDaoImpl;

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 3;
	private static final int[] PAGE_SIZES = { 3, 5, 10, 25, 50, 75, 100 };

	public Bagian save(Bagian bagian) {
		return bagianDaoImpl.save(bagian);
	}

	public Bagian edit(Bagian bagian) {
		return bagianDaoImpl.save(bagian);
	}

	public void delete(Long id) {
		bagianDaoImpl.delete(id);
	}

	public Bagian findById(Long id) {
		return bagianDaoImpl.findById(id);
	}
	
	public BagianDto dtoFindById(Long id) {
		return bagianDaoImpl.dtoFindById(id).get();
	}

	public ModelDataTable<BagianDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");
		Page<BagianDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = bagianDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = bagianDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection),
					evalSearch);
		}

		PagerModel pager = new PagerModel(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
		ModelDataTable<BagianDto> model = new ModelDataTable<>();
		model.setData(pages);
		model.setEvalPage(evalPage);
		model.setPagerModel(pager);
		model.setPageSize(PAGE_SIZES);
		model.setSelectedPageSize(evalPageSize);
//		model.setTotalRow(bagianDaoImpl.findAll().size());
		model.setResultCount(pages.getSize());
//		model.setAuthName(auth.getAuthentication().getName());

		return model;
	}

	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {

		String type = req.getParameter("type");

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");

		Page<BagianDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = bagianDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = bagianDaoImpl.pagination(
					PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection), evalSearch);
		}

		List<BagianDto> lists = pages.getContent();

		int number = pages.getNumber();
		int size = pages.getSize();

		Map<String, Object> mapModel = new HashMap<>();
		mapModel.put("reports", lists);
		mapModel.put("number", number);
		mapModel.put("size", size);

		ExcelBagianReport excel = new ExcelBagianReport();
		PdfBagianReport pdf = new PdfBagianReport();
		if (type.equals("xls")) {
			return new ModelAndView(excel, mapModel);
		} else if (type.equals("pdf")) {
			return new ModelAndView(pdf, mapModel);
		}
		return new ModelAndView(new ExcelBagianReport(), mapModel);
	}
}
