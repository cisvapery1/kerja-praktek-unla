package com.pripurna.service;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.pripurna.dao.UserDao;
import com.pripurna.dto.UserDto;
import com.pripurna.model.User;
import com.pripurna.report.excel.ExcelUserReport;
import com.pripurna.report.pdf.PdfUserReport;
import com.pripurna.util.ModelDataTable;
import com.pripurna.util.PagerModel;

@Service
@Transactional
public class UserService {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 3;
	private static final int[] PAGE_SIZES = { 3, 5, 10, 25, 50, 75, 100 };
	
	@Autowired
	private UserDao userDaoImpl; 
	
	public User save(User user) {
		return userDaoImpl.save(user);
	}

	public User edit(User user) {
		return userDaoImpl.save(user);
	}

	public void delete(Long id) {
		userDaoImpl.delete(id);
	}

	public User findById(Long id) {
		return userDaoImpl.findById(id);
	}
	
	public UserDto dtoFindById(Long id) {
		return userDaoImpl.dtoFindById(id).get();
	}

	public ModelDataTable<UserDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");
		Page<UserDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = userDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = userDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection),
					evalSearch);
		}

		PagerModel pager = new PagerModel(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
		ModelDataTable<UserDto> model = new ModelDataTable<>();
		model.setData(pages);
		model.setEvalPage(evalPage);
		model.setPagerModel(pager);
		model.setPageSize(PAGE_SIZES);
		model.setSelectedPageSize(evalPageSize);
//		model.setTotalRow(bagianDaoImpl.findAll().size());
		model.setResultCount(pages.getSize());
//		model.setAuthName(auth.getAuthentication().getName());

		return model;
	}

	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {

		String type = req.getParameter("type");

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");

		Page<UserDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = userDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = userDaoImpl.pagination(
					PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection), evalSearch);
		}

		List<UserDto> lists = pages.getContent();

		int number = pages.getNumber();
		int size = pages.getSize();

		Map<String, Object> mapModel = new HashMap<>();
		mapModel.put("reports", lists);
		mapModel.put("number", number);
		mapModel.put("size", size);

		ExcelUserReport excel = new ExcelUserReport();
		PdfUserReport pdf = new PdfUserReport();
		if (type.equals("xls")) {
			return new ModelAndView(excel, mapModel);
		} else if (type.equals("pdf")) {
			return new ModelAndView(pdf, mapModel);
		}
		return new ModelAndView(new ExcelUserReport(), mapModel);
	}
}
