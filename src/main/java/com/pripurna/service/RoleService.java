package com.pripurna.service;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.pripurna.dao.impl.RoleDaoImpl;
import com.pripurna.dto.RoleDto;
import com.pripurna.model.Role;
import com.pripurna.report.excel.ExcelRoleReport;
import com.pripurna.report.pdf.PdfRoleReport;
import com.pripurna.util.ModelDataTable;
import com.pripurna.util.PagerModel;

@Service
@Transactional
public class RoleService {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 3;
	private static final int[] PAGE_SIZES = { 3, 5, 10, 25, 50, 75, 100 };
	
	@Autowired
	private RoleDaoImpl roleDaoImpl;

	public Role save(Role role) {
		return roleDaoImpl.save(role);
	}

	public Role edit(Role role) {
		return roleDaoImpl.save(role);
	}

	public void delete(Long id) {
		roleDaoImpl.delete(id);
	}

	public Role findById(Long id) {
		return roleDaoImpl.findById(id);
	}
	
	public RoleDto dtoFindById(Long id) {
		return roleDaoImpl.dtoFindById(id).get();
	}

	public ModelDataTable<RoleDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");
		Page<RoleDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = roleDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = roleDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection),
					evalSearch);
		}

		PagerModel pager = new PagerModel(pages.getTotalPages(), pages.getNumber(), BUTTONS_TO_SHOW);
		ModelDataTable<RoleDto> model = new ModelDataTable<>();
		model.setData(pages);
		model.setEvalPage(evalPage);
		model.setPagerModel(pager);
		model.setPageSize(PAGE_SIZES);
		model.setSelectedPageSize(evalPageSize);
//		model.setTotalRow(bagianDaoImpl.findAll().size());
		model.setResultCount(pages.getSize());
//		model.setAuthName(auth.getAuthentication().getName());

		return model;
	}

	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {

		String type = req.getParameter("type");

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String evalSearch = search.orElse("");
		String evalSortBy = sortBy.orElse("ASC");
		String evalsortDirection = sortDirection.orElse("id");

		Page<RoleDto> pages = null;

		Direction direction;
		if (evalSortBy.equals("ASC")) {
			direction = Sort.Direction.ASC;
		} else {
			direction = Sort.Direction.DESC;
		}

		if (evalSearch.equals("")) {
			pages = roleDaoImpl.pagination(PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection));
		} else {
			pages = roleDaoImpl.pagination(
					PageRequest.of(evalPage, evalPageSize, direction, evalsortDirection), evalSearch);
		}

		List<RoleDto> lists = pages.getContent();

		int number = pages.getNumber();
		int size = pages.getSize();

		Map<String, Object> mapModel = new HashMap<>();
		mapModel.put("reports", lists);
		mapModel.put("number", number);
		mapModel.put("size", size);

		ExcelRoleReport excel = new ExcelRoleReport();
		PdfRoleReport pdf = new PdfRoleReport();
		if (type.equals("xls")) {
			return new ModelAndView(excel, mapModel);
		} else if (type.equals("pdf")) {
			return new ModelAndView(pdf, mapModel);
		}
		return new ModelAndView(new ExcelRoleReport(), mapModel);
	}
	
}
