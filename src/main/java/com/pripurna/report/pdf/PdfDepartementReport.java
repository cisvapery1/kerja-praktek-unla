package com.pripurna.report.pdf;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.pripurna.dto.DepartementDto;
import com.pripurna.report.AbstractITextPdfView;
import com.pripurna.report.StylePdfHeaderAndFooter;

public class PdfDepartementReport extends AbstractITextPdfView {

	public PdfDepartementReport() {
	}

	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document doc, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		doc.setPageSize(PageSize.A4);
		doc.setMargins(36, 36, 80, 40);
		StylePdfHeaderAndFooter event = new StylePdfHeaderAndFooter();
		writer.setBoxSize("art", new Rectangle(36, 54, 559, 788));
		writer.setPageEvent(event);
		@SuppressWarnings("unchecked")
		List<DepartementDto> list = (List<DepartementDto>) model.get("reports");
		Integer number = (Integer) model.get("number");
		Integer size = (Integer) model.get("size");
		Font f = new Font(FontFamily.TIMES_ROMAN, 25.0f, Font.BOLD, BaseColor.BLACK);
		Chunk c = new Chunk("Departement", f);
		Paragraph p = new Paragraph(c);

		p.setSpacingAfter(10);
		p.setAlignment(Paragraph.ALIGN_CENTER);

		doc.add(p);
		doc.addTitle("Departement");

		PdfPTable table = new PdfPTable(4);
		table.getDefaultCell().setBorderWidth(0.3f);
		table.setSkipFirstHeader(true);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.setWidthPercentage(100.0f);
		table.setWidths(new float[] { 1f, 2.0f, 4f, 3.0f });
		table.setSpacingBefore(10);

		// define font for table header row
		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setColor(BaseColor.WHITE);

		if (!list.isEmpty()) {

			// define table header cell
			PdfPCell cell = new PdfPCell();
			cell.setPadding(10);
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(BaseColor.DARK_GRAY);

			// write table header
			cell.setPhrase(new Phrase("No", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Nama", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Deskripsi", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Tanggal di buat", font));
			table.addCell(cell);

			int i = 0;
			// write table row data
			Font fontCell = FontFactory.getFont(FontFactory.HELVETICA);
			fontCell.setColor(BaseColor.BLACK);
			fontCell.setSize(10f);

			PdfPCell cellRow = new PdfPCell();
			cellRow.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellRow.setPadding(10);
			cellRow.setBorder(Rectangle.NO_BORDER);

			for (DepartementDto o : list) {

				if (i % 2 == 0) {
					cellRow.setBackgroundColor(BaseColor.WHITE);
				} else {
					cellRow.setBackgroundColor(BaseColor.CYAN);
				}

				cellRow.setPhrase(new Phrase(String.valueOf((number * size) + (i + 1)), fontCell));
				table.addCell(cellRow);

				cellRow.setPhrase(new Phrase(o.getName(), fontCell));
				table.addCell(cellRow);

				cellRow.setPhrase(new Phrase(o.getDeskripsi(), fontCell));
				table.addCell(cellRow);

				cellRow.setPhrase(new Phrase(o.getFormatCreatedDate(), fontCell));
				table.addCell(cellRow);

				i++;
			}

			doc.add(table);
		} else {

			// define table header cell
			PdfPCell cell = new PdfPCell();
			cell.setPadding(10);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(BaseColor.DARK_GRAY);

			// write table header
			cell.setPhrase(new Phrase("No", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Nama", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Deskripsi", font));
			table.addCell(cell);

			cell.setPhrase(new Phrase("Tanggal di buat", font));
			table.addCell(cell);

			// style change table
			table.getDefaultCell().setBorderWidth(1f);
			table.getDefaultCell().setBorder(Rectangle.BOX);

			PdfPCell cellNoData = new PdfPCell();
			cellNoData.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellNoData.setPadding(10);

			// write table row data
			Font fontCell = FontFactory.getFont(FontFactory.HELVETICA);
			fontCell.setColor(BaseColor.BLACK);
			fontCell.setSize(10f);

			fontCell = FontFactory.getFont(FontFactory.HELVETICA);
			fontCell.setColor(BaseColor.BLACK);
			fontCell.setSize(10f);
			cellNoData.setPhrase(new Phrase("No data", fontCell));
			cellNoData.setColspan(4);

			table.addCell(cellNoData);
			doc.add(table);
		}
	}

}