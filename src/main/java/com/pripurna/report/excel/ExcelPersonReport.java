package com.pripurna.report.excel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.pripurna.dto.PersonDto;


public class ExcelPersonReport extends AbstractXlsView{

	public ExcelPersonReport() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		response.setHeader("Content-Disposition", "attachment; filename=\"Report.xls\"");

		@SuppressWarnings("unchecked")
		List<PersonDto> lists = (List<PersonDto>) model.get("reports");
		Integer number = (Integer) model.get("number");
		Integer size = (Integer) model.get("size");
		
		Sheet s = workbook.createSheet("Report");

		Row row = s.createRow(0);
		row.createCell(0).setCellValue("No");
		row.createCell(1).setCellValue("NIK");
		row.createCell(2).setCellValue("Nama");
		row.createCell(3).setCellValue("Tanggal Lahir");
		row.createCell(4).setCellValue("Alamat");
		row.createCell(5).setCellValue("Telepon");
		row.createCell(6).setCellValue("Agama");
		row.createCell(7).setCellValue("Status Perkawinan");
		row.createCell(8).setCellValue("Kewarganegaraan");

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setColor(IndexedColors.WHITE.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setFillBackgroundColor(IndexedColors.BLUE.getIndex());
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerCellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		row.getCell(0).setCellStyle(headerCellStyle);
		row.getCell(1).setCellStyle(headerCellStyle);
		row.getCell(2).setCellStyle(headerCellStyle);
		row.getCell(3).setCellStyle(headerCellStyle);
		row.getCell(4).setCellStyle(headerCellStyle);
		row.getCell(5).setCellStyle(headerCellStyle);
		row.getCell(6).setCellStyle(headerCellStyle);
		row.getCell(7).setCellStyle(headerCellStyle);
		row.getCell(8).setCellStyle(headerCellStyle);
		
		int rowNum = 1;
		int i = 0;
		for (PersonDto o : lists) {
			Row r = s.createRow(rowNum++);
			r.createCell(0).setCellValue(String.valueOf((number*size) + (i+1)));
			r.createCell(1).setCellValue(o.getNik());
			r.createCell(2).setCellValue(o.getName());
			r.createCell(3).setCellValue(o.getFormatTanggalLahir());
			r.createCell(4).setCellValue(o.getAlamat());
			r.createCell(5).setCellValue(o.getTelepon());
			r.createCell(6).setCellValue(o.getAgama());
			r.createCell(7).setCellValue(o.getStatusPerkawinan());
			r.createCell(8).setCellValue(o.getKewarganegaraan());
			i++;
		}
		
		s.autoSizeColumn(0);
		s.autoSizeColumn(1);
		s.autoSizeColumn(2);
		s.autoSizeColumn(3);
		s.autoSizeColumn(4);
		s.autoSizeColumn(5);
		s.autoSizeColumn(6);
		s.autoSizeColumn(7);
		s.autoSizeColumn(8);
	}

}
