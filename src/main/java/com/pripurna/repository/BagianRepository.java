package com.pripurna.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pripurna.dto.BagianDto;
import com.pripurna.model.Bagian;

public interface BagianRepository extends JpaRepository<Bagian, Long>{
	
	@Query("select new com.pripurna.dto.BagianDto(o) from Bagian o order by o.createdDate desc")
	Page<BagianDto> pagination(Pageable pagebale);
	
	@Query("select new com.pripurna.dto.BagianDto(o) from Bagian o "
			+ "where o.name like %?1% "
			+ "order by o.createdDate desc")
	Page<BagianDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.pripurna.dto.BagianDto(o) from Bagian o where o.id = ?1 ")
	Optional<BagianDto> dtoFindById(Long id);
	
	List<Bagian> findByDepartementId(Long id);
}
