package com.pripurna.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pripurna.dto.PegawaiCutiDto;
import com.pripurna.model.PegawaiCuti;

public interface PegawaiCutiRepository extends JpaRepository<PegawaiCuti, Long>{

	@Query("select new com.pripurna.dto.PegawaiCutiDto(o) from PegawaiCuti o order by o.createdDate desc")
	Page<PegawaiCutiDto> pagination(Pageable pagebale);
	
	@Query("select new com.pripurna.dto.PegawaiCutiDto(o) from PegawaiCuti o "
			+ "where o.status like %?1% or  o.body like %?1%"
			+ "order by o.createdDate desc")
	Page<PegawaiCutiDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.pripurna.dto.PegawaiCutiDto(o) from PegawaiCuti o where o.id = ?1 ")
	Optional<PegawaiCutiDto> dtoFindById(Long id);
}
