package com.pripurna.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pripurna.dto.PersonDto;
import com.pripurna.model.Person;

public interface PersonRepository extends JpaRepository<Person, Long>{

	@Query("select new com.pripurna.dto.PersonDto(o) from Person o order by o.createdDate desc")
	Page<PersonDto> pagination(Pageable pagebale);
	
	@Query("select new com.pripurna.dto.PersonDto(o) from Person o "
			+ "where o.nik like %?1% or o.name like %?1% or o.telepon like %?1% "
			+ "order by o.createdDate desc")
	Page<PersonDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.pripurna.dto.PersonDto(o) from Person o where o.id = ?1 ")
	Optional<PersonDto> dtoFindById(Long id);
}