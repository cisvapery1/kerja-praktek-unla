package com.pripurna.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pripurna.dto.CutiDto;
import com.pripurna.model.Cuti;

public interface CutiRepository extends JpaRepository<Cuti, Long>{

	@Query("select new com.pripurna.dto.CutiDto(o) from Cuti o order by o.createdDate desc")
	Page<CutiDto> pagination(Pageable pagebale);
	
	@Query("select new com.pripurna.dto.CutiDto(o) from Cuti o "
			+ "where o.type like %?1% or o.jumlahCutiTahunan like %?1% "
			+ "order by o.createdDate desc")
	Page<CutiDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.pripurna.dto.CutiDto(o) from Cuti o where o.id = ?1 ")
	Optional<CutiDto> dtoFindById(Long id);
}
