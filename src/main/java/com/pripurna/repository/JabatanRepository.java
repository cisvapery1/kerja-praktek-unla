package com.pripurna.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pripurna.dto.JabatanDto;
import com.pripurna.model.Jabatan;

public interface JabatanRepository extends JpaRepository<Jabatan, Long>{

	@Query("select new com.pripurna.dto.JabatanDto(o) from Jabatan o order by o.createdDate desc")
	Page<JabatanDto> pagination(Pageable pagebale);
	
	@Query("select new com.pripurna.dto.JabatanDto(o) from Jabatan o "
			+ "where o.name like %?1% "
			+ "order by o.createdDate desc")
	Page<JabatanDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.pripurna.dto.JabatanDto(o) from Jabatan o where o.id = ?1 ")
	Optional<JabatanDto> dtoFindById(Long id);
}
