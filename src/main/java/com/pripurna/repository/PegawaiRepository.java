package com.pripurna.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pripurna.dto.PegawaiDto;
import com.pripurna.model.Pegawai;

public interface PegawaiRepository extends JpaRepository<Pegawai, Long>{

	@Query("select new com.pripurna.dto.PegawaiDto(o) from Pegawai o order by o.createdDate desc")
	Page<PegawaiDto> pagination(Pageable pagebale);
	
	@Query("select new com.pripurna.dto.PegawaiDto(o) from Pegawai o "
			+ "where o.npwp like %?1% "
			+ "order by o.createdDate desc")
	Page<PegawaiDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.pripurna.dto.PegawaiDto(o) from Pegawai o where o.id = ?1 ")
	Optional<PegawaiDto> dtoFindById(Long id);
}
