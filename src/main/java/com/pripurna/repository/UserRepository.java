package com.pripurna.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.pripurna.dto.UserDto;
import com.pripurna.model.User;
import java.lang.String;
import java.util.Optional;

@Component
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsernameOrEmail(String username, String email);

	Optional<User> findByUsername(String username);
	
	Optional<User> findByEmail(String email);

	@Query("select new com.pripurna.dto.UserDto(o) from User o order by o.createdDate desc")
	Page<UserDto> pagination(Pageable pagebale);
	
	@Query("select new com.pripurna.dto.UserDto(o) from User o "
			+ "where o.username like %?1% or o.email like %?1% "
			+ "order by o.createdDate desc")
	Page<UserDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.pripurna.dto.UserDto(o) from User o where o.id = ?1 ")
	Optional<UserDto> dtoFindById(Long id);
}
