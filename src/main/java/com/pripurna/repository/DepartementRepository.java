package com.pripurna.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pripurna.dto.DepartementDto;
import com.pripurna.model.Departement;

public interface DepartementRepository extends JpaRepository<Departement, Long>{

	@Query("select new com.pripurna.dto.DepartementDto(o) from Departement o order by o.createdDate desc")
	Page<DepartementDto> pagination(Pageable pagebale);
	
	@Query("select new com.pripurna.dto.DepartementDto(o) from Departement o "
			+ "where o.name like %?1% "
			+ "order by o.createdDate desc")
	Page<DepartementDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.pripurna.dto.DepartementDto(o) from Departement o where o.id = ?1 ")
	Optional<DepartementDto> dtoFindById(Long id);
}
