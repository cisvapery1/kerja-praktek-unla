package com.pripurna.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pripurna.dto.RoleDto;
import com.pripurna.model.Role;
import java.lang.String;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{

	Role findByName(String name);
	
	@Query("select new com.pripurna.dto.RoleDto(o) from Role o order by o.createdDate desc")
	Page<RoleDto> pagination(Pageable pagebale);
	
	@Query("select new com.pripurna.dto.RoleDto(o) from Role o "
			+ "where o.name like %?1% "
			+ "order by o.createdDate desc")
	Page<RoleDto> pagination(Pageable pageabale, String search);
	
	@Query("select new com.pripurna.dto.RoleDto(o) from Role o where o.id = ?1 ")
	Optional<RoleDto> dtoFindById(Long id);
}