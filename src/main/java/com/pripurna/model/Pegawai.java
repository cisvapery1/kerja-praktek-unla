package com.pripurna.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.pripurna.model.audit.Auditable;

@Entity
@Table(name = "PEGAWAI")
public class Pegawai extends Auditable<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String npwp;

	@Column
	private Date tanggalMasuk;

	@Column
	private String statusPekerjaan;

	@Column
	private String pendidikanTerakhir;

	@Column
	private Integer limitCutiTahunan;

	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "PERSON_ID", referencedColumnName = "id")
	private Person person;

	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "JABATAN_ID", nullable = false)
	private Jabatan jabatan;

	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "BAGIAN_ID", referencedColumnName = "id")
	private Bagian bagian;

	@OneToMany(mappedBy = "pegawai", cascade = CascadeType.ALL)
	private Set<PegawaiCuti> pegawaiCutis;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public Date getTanggalMasuk() {
		return tanggalMasuk;
	}

	public void setTanggalMasuk(Date tanggalMasuk) {
		this.tanggalMasuk = tanggalMasuk;
	}

	public String getStatusPekerjaan() {
		return statusPekerjaan;
	}

	public void setStatusPekerjaan(String statusPekerjaan) {
		this.statusPekerjaan = statusPekerjaan;
	}

	public String getPendidikanTerakhir() {
		return pendidikanTerakhir;
	}

	public void setPendidikanTerakhir(String pendidikanTerakhir) {
		this.pendidikanTerakhir = pendidikanTerakhir;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Jabatan getJabatan() {
		return jabatan;
	}

	public void setJabatan(Jabatan jabatan) {
		this.jabatan = jabatan;
	}

	public Bagian getBagian() {
		return bagian;
	}

	public void setBagian(Bagian bagian) {
		this.bagian = bagian;
	}

	public Set<PegawaiCuti> getPegawaiCutis() {
		return pegawaiCutis;
	}

	public void setPegawaiCutis(Set<PegawaiCuti> pegawaiCutis) {
		this.pegawaiCutis = pegawaiCutis;
	}

	public Integer getLimitCutiTahunan() {
		return limitCutiTahunan;
	}

	public void setLimitCutiTahunan(Integer limitCutiTahunan) {
		this.limitCutiTahunan = limitCutiTahunan;
	}

}