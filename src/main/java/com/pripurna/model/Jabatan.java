package com.pripurna.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.pripurna.model.audit.Auditable;

@Entity
@Table(name = "JABATAN")
public class Jabatan extends Auditable<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String name;

	@Column
	private String deskripsi;

	@OneToMany(mappedBy = "jabatan", cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Set<Pegawai> pegawai;

	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.PERSIST, CascadeType.REFRESH }, targetEntity = Cuti.class)
	@JoinColumn(name = "cuti_id")
	private Cuti cuti;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public Set<Pegawai> getPegawai() {
		return pegawai;
	}

	public void setPegawai(Set<Pegawai> pegawai) {
		this.pegawai = pegawai;
	}

	public Cuti getCuti() {
		return cuti;
	}

	public void setCuti(Cuti cuti) {
		this.cuti = cuti;
	}

}
