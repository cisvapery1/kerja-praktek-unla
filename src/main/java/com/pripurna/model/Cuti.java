package com.pripurna.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.pripurna.model.audit.Auditable;

@Entity
@Table(name = "CUTI")
public class Cuti extends Auditable<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String type;

	@Column
	private Integer jumlahCutiTahunan;

	@OneToMany(mappedBy = "cuti", fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.PERSIST, CascadeType.REFRESH })
	private Set<Jabatan> jabatans;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getJumlahCutiTahunan() {
		return jumlahCutiTahunan;
	}

	public void setJumlahCutiTahunan(Integer jumlahCutiTahunan) {
		this.jumlahCutiTahunan = jumlahCutiTahunan;
	}

	public Set<Jabatan> getJabatans() {
		return jabatans;
	}

	public void setJabatans(Set<Jabatan> jabatans) {
		this.jabatans = jabatans;
	}

}
