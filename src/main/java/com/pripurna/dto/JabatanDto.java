package com.pripurna.dto;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.pripurna.model.Jabatan;
import com.pripurna.util.LocalDateUtil;

public class JabatanDto {

	private Long id;
	private String name;
	private String deskripsi;

	private String createdBy;
	private Date createdDate;
	private String lastModifiedBy;
	private Date lastModifiedDate;
	private String formatCreatedDate;
	private String formatLastModifiedDate;
	private LocalDateUtil localDateUtil;

	private CutiDto cuti;

	public JabatanDto() {
		super();
	}

	public JabatanDto(Jabatan obj) {
		this.id = obj.getId();
		this.name = obj.getName();
		this.deskripsi = obj.getDeskripsi();
		this.cuti = new CutiDto(obj.getCuti());
		this.createdBy = obj.getCreatedBy();
		this.createdDate = obj.getCreatedDate();
		this.lastModifiedBy = obj.getLastModifiedBy();
		this.lastModifiedDate = obj.getLastModifiedDate();

		localDateUtil = new LocalDateUtil();
		this.formatCreatedDate = localDateUtil.formatDate2(createdDate);
		this.formatLastModifiedDate = localDateUtil.formatDate2(lastModifiedDate);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFormatCreatedDate() {
		return formatCreatedDate;
	}

	public void setFormatCreatedDate(String formatCreatedDate) {
		this.formatCreatedDate = formatCreatedDate;
	}

	public String getFormatLastModifiedDate() {
		return formatLastModifiedDate;
	}

	public void setFormatLastModifiedDate(String formatLastModifiedDate) {
		this.formatLastModifiedDate = formatLastModifiedDate;
	}

	public LocalDateUtil getLocalDateUtil() {
		return localDateUtil;
	}

	public void setLocalDateUtil(LocalDateUtil localDateUtil) {
		this.localDateUtil = localDateUtil;
	}

	public CutiDto getCuti() {
		return cuti;
	}

	public void setCuti(CutiDto cuti) {
		this.cuti = cuti;
	}

}
