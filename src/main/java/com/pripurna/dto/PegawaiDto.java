package com.pripurna.dto;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.pripurna.model.Pegawai;
import com.pripurna.util.LocalDateUtil;

public class PegawaiDto {

	private Long id;
	private String npwp;
	private Date tanggalMasuk;
	private String statusPekerjaan;
	private String pendidikanTerakhir;

	private PersonDto person;
	private JabatanDto jabatan;
	private BagianDto bagian;

	private String createdBy;
	private Date createdDate;
	private String lastModifiedBy;
	private Date lastModifiedDate;
	private String formatCreatedDate;
	private String formatLastModifiedDate;
	private String formatTanggalMasuk;
	@Autowired
	private LocalDateUtil localDateUtil;

	public PegawaiDto() {
		super();
	}

	public PegawaiDto(Pegawai obj) {
		this.id = obj.getId();
		this.npwp = obj.getNpwp();
		this.tanggalMasuk = obj.getTanggalMasuk();
		this.statusPekerjaan = obj.getStatusPekerjaan();
		this.pendidikanTerakhir = obj.getPendidikanTerakhir();

		this.person = new PersonDto(obj.getPerson());
		this.jabatan = new JabatanDto(obj.getJabatan());
		this.bagian = new BagianDto(obj.getBagian());

		this.createdBy = obj.getCreatedBy();
		this.createdDate = obj.getCreatedDate();
		this.lastModifiedBy = obj.getLastModifiedBy();
		this.lastModifiedDate = obj.getLastModifiedDate();

		this.formatCreatedDate = localDateUtil.formatDate2(createdDate);
		this.formatLastModifiedDate = localDateUtil.formatDate2(lastModifiedDate);
		this.formatTanggalMasuk = localDateUtil.formatDate(tanggalMasuk);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public Date getTanggalMasuk() {
		return tanggalMasuk;
	}

	public void setTanggalMasuk(Date tanggalMasuk) {
		this.tanggalMasuk = tanggalMasuk;
	}

	public String getStatusPekerjaan() {
		return statusPekerjaan;
	}

	public void setStatusPekerjaan(String statusPekerjaan) {
		this.statusPekerjaan = statusPekerjaan;
	}

	public String getPendidikanTerakhir() {
		return pendidikanTerakhir;
	}

	public void setPendidikanTerakhir(String pendidikanTerakhir) {
		this.pendidikanTerakhir = pendidikanTerakhir;
	}

	public PersonDto getPerson() {
		return person;
	}

	public void setPerson(PersonDto person) {
		this.person = person;
	}

	public JabatanDto getJabatan() {
		return jabatan;
	}

	public void setJabatan(JabatanDto jabatan) {
		this.jabatan = jabatan;
	}

	public BagianDto getBagian() {
		return bagian;
	}

	public void setBagian(BagianDto bagian) {
		this.bagian = bagian;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFormatCreatedDate() {
		return formatCreatedDate;
	}

	public void setFormatCreatedDate(String formatCreatedDate) {
		this.formatCreatedDate = formatCreatedDate;
	}

	public String getFormatLastModifiedDate() {
		return formatLastModifiedDate;
	}

	public void setFormatLastModifiedDate(String formatLastModifiedDate) {
		this.formatLastModifiedDate = formatLastModifiedDate;
	}

	public String getFormatTanggalMasuk() {
		return formatTanggalMasuk;
	}

	public void setFormatTanggalMasuk(String formatTanggalMasuk) {
		this.formatTanggalMasuk = formatTanggalMasuk;
	}

	public LocalDateUtil getLocalDateUtil() {
		return localDateUtil;
	}

	public void setLocalDateUtil(LocalDateUtil localDateUtil) {
		this.localDateUtil = localDateUtil;
	}

}
