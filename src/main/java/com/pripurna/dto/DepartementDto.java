package com.pripurna.dto;

import java.util.Date;

import com.pripurna.model.Departement;
import com.pripurna.util.LocalDateUtil;

public class DepartementDto {

	private Long id;
	private String name;
	private String deskripsi;
	
	private String createdBy;
	private Date createdDate;
	private String lastModifiedBy;
	private Date lastModifiedDate;
	
	private String formatCreatedDate;
	private String formatLastModifiedDate;
	
	
	private LocalDateUtil localDateUtil;

	public DepartementDto() {
		super();
	}
	
	public DepartementDto(Departement obj) {
		this.id = obj.getId();
		this.name = obj.getName();
		this.deskripsi = obj.getDeskripsi();

		this.createdBy = obj.getCreatedBy();
		this.createdDate = obj.getCreatedDate();
		this.lastModifiedBy = obj.getLastModifiedBy();
		this.lastModifiedDate = obj.getLastModifiedDate();

		localDateUtil = new LocalDateUtil();
		this.formatCreatedDate = localDateUtil.formatDate2(createdDate);
		this.formatLastModifiedDate = localDateUtil.formatDate2(lastModifiedDate);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFormatCreatedDate() {
		return formatCreatedDate;
	}

	public void setFormatCreatedDate(String formatCreatedDate) {
		this.formatCreatedDate = formatCreatedDate;
	}

	public String getFormatLastModifiedDate() {
		return formatLastModifiedDate;
	}

	public void setFormatLastModifiedDate(String formatLastModifiedDate) {
		this.formatLastModifiedDate = formatLastModifiedDate;
	}

	public LocalDateUtil getLocalDateUtil() {
		return localDateUtil;
	}

	public void setLocalDateUtil(LocalDateUtil localDateUtil) {
		this.localDateUtil = localDateUtil;
	}
	
	
}
