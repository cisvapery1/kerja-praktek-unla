package com.pripurna.dto;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.pripurna.model.PegawaiCuti;
import com.pripurna.util.LocalDateUtil;

public class PegawaiCutiDto {

	private Long id;
	private boolean approved;
	private String status;
	private String body;
	private String fileName;

	private PegawaiDto pegawai;

	private String createdBy;
	private Date createdDate;
	private String lastModifiedBy;
	private Date lastModifiedDate;

	private String formatCreatedDate;
	private String formatLastModifiedDate;

	@Autowired
	private LocalDateUtil localDateUtil;

	public PegawaiCutiDto() {
		super();
	}

	public PegawaiCutiDto(PegawaiCuti obj) {
		this.id = obj.getId();
		this.approved = obj.isApproved();
		this.status = obj.getStatus();
		this.body = obj.getBody();
		this.fileName = obj.getFileName();
		this.pegawai = new PegawaiDto(obj.getPegawai());

		this.createdBy = obj.getCreatedBy();
		this.createdDate = obj.getCreatedDate();
		this.lastModifiedBy = obj.getLastModifiedBy();
		this.lastModifiedDate = obj.getLastModifiedDate();

		this.formatCreatedDate = localDateUtil.formatDate2(createdDate);
		this.formatLastModifiedDate = localDateUtil.formatDate2(lastModifiedDate);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public PegawaiDto getPegawai() {
		return pegawai;
	}

	public void setPegawai(PegawaiDto pegawai) {
		this.pegawai = pegawai;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFormatCreatedDate() {
		return formatCreatedDate;
	}

	public void setFormatCreatedDate(String formatCreatedDate) {
		this.formatCreatedDate = formatCreatedDate;
	}

	public String getFormatLastModifiedDate() {
		return formatLastModifiedDate;
	}

	public void setFormatLastModifiedDate(String formatLastModifiedDate) {
		this.formatLastModifiedDate = formatLastModifiedDate;
	}

	public LocalDateUtil getLocalDateUtil() {
		return localDateUtil;
	}

	public void setLocalDateUtil(LocalDateUtil localDateUtil) {
		this.localDateUtil = localDateUtil;
	}

}
