package com.pripurna.dto;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.pripurna.model.Person;
import com.pripurna.util.LocalDateUtil;

public class PersonDto {

	private Long id;
	private String nik;
	private String name;
	private Date tanggalLahir;
	private String alamat;
	private String telepon;
	private String agama;
	private String statusPerkawinan;
	private String kewarganegaraan;

	private UserDto user;
	private PegawaiDto pegawai;

	private String createdBy;
	private Date createdDate;
	private String lastModifiedBy;
	private Date lastModifiedDate;

	private String formatCreatedDate;
	private String formatLastModifiedDate;
	private String formatTanggalLahir;

	@Autowired
	private LocalDateUtil localDateUtil;

	public PersonDto() {
		super();
	}

	public PersonDto(Person obj) {
		this.id = obj.getId();
		this.nik = obj.getNik();
		this.name = obj.getName();
		this.tanggalLahir = obj.getTanggalLahir();
		this.alamat = obj.getAlamat();
		this.telepon = obj.getTelepon();
		this.agama = obj.getAgama();
		this.statusPerkawinan = obj.getStatusPerkawinan();
		this.kewarganegaraan = obj.getKewarganegaraan();

		this.user = new UserDto(obj.getUser());
		this.pegawai = new PegawaiDto(obj.getPegawai());

		this.createdBy = obj.getCreatedBy();
		this.createdDate = obj.getCreatedDate();
		this.lastModifiedBy = obj.getLastModifiedBy();
		this.lastModifiedDate = obj.getLastModifiedDate();

		this.formatCreatedDate = localDateUtil.formatDate2(createdDate);
		this.formatLastModifiedDate = localDateUtil.formatDate2(lastModifiedDate);
		this.formatTanggalLahir = localDateUtil.formatDate(tanggalLahir);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getTelepon() {
		return telepon;
	}

	public void setTelepon(String telepon) {
		this.telepon = telepon;
	}

	public String getAgama() {
		return agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public String getStatusPerkawinan() {
		return statusPerkawinan;
	}

	public void setStatusPerkawinan(String statusPerkawinan) {
		this.statusPerkawinan = statusPerkawinan;
	}

	public String getKewarganegaraan() {
		return kewarganegaraan;
	}

	public void setKewarganegaraan(String kewarganegaraan) {
		this.kewarganegaraan = kewarganegaraan;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public PegawaiDto getPegawai() {
		return pegawai;
	}

	public void setPegawai(PegawaiDto pegawai) {
		this.pegawai = pegawai;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getFormatCreatedDate() {
		return formatCreatedDate;
	}

	public void setFormatCreatedDate(String formatCreatedDate) {
		this.formatCreatedDate = formatCreatedDate;
	}

	public String getFormatLastModifiedDate() {
		return formatLastModifiedDate;
	}

	public void setFormatLastModifiedDate(String formatLastModifiedDate) {
		this.formatLastModifiedDate = formatLastModifiedDate;
	}

	public String getFormatTanggalLahir() {
		return formatTanggalLahir;
	}

	public void setFormatTanggalLahir(String formatTanggalLahir) {
		this.formatTanggalLahir = formatTanggalLahir;
	}

	public LocalDateUtil getLocalDateUtil() {
		return localDateUtil;
	}

	public void setLocalDateUtil(LocalDateUtil localDateUtil) {
		this.localDateUtil = localDateUtil;
	}

}
