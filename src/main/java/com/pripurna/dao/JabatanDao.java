package com.pripurna.dao;

import com.pripurna.dto.JabatanDto;
import com.pripurna.model.Jabatan;

public interface JabatanDao extends CrudDao<Jabatan>, PaginationDao<JabatanDto>, ModelDto<JabatanDto>{
}