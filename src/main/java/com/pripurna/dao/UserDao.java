package com.pripurna.dao;

import com.pripurna.dto.UserDto;
import com.pripurna.model.User;

public interface UserDao extends CrudDao<User>, PaginationDao<UserDto>, ModelDto<UserDto>{
}