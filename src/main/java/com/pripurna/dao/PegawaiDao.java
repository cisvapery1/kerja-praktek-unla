package com.pripurna.dao;

import com.pripurna.dto.PegawaiDto;
import com.pripurna.model.Pegawai;

public interface PegawaiDao extends CrudDao<Pegawai>, PaginationDao<PegawaiDto>, ModelDto<PegawaiDto> {

}
