package com.pripurna.dao;

import com.pripurna.dto.BagianDto;
import com.pripurna.model.Bagian;

public interface BagianDao extends CrudDao<Bagian>, PaginationDao<BagianDto>, ModelDto<BagianDto>{
}