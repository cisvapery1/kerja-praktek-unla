package com.pripurna.dao;

import com.pripurna.dto.DepartementDto;
import com.pripurna.model.Departement;

public interface DepartementDao extends CrudDao<Departement>, PaginationDao<DepartementDto>, ModelDto<DepartementDto>{
}