package com.pripurna.dao;

import java.util.Optional;

public interface ModelDto<T> {

	public Optional<T> dtoFindById(Long id);
	
}
