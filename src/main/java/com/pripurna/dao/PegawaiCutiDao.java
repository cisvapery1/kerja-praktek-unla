package com.pripurna.dao;

import com.pripurna.dto.PegawaiCutiDto;
import com.pripurna.model.PegawaiCuti;

public interface PegawaiCutiDao extends CrudDao<PegawaiCuti>, PaginationDao<PegawaiCutiDto>, ModelDto<PegawaiCutiDto> {
}
