package com.pripurna.dao;

import com.pripurna.dto.PersonDto;
import com.pripurna.model.Person;

public interface PersonDao extends CrudDao<Person>, PaginationDao<PersonDto>, ModelDto<PersonDto>{

}
