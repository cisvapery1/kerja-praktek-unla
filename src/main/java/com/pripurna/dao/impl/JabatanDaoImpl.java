package com.pripurna.dao.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.pripurna.dao.JabatanDao;
import com.pripurna.dto.JabatanDto;
import com.pripurna.model.Jabatan;
import com.pripurna.repository.JabatanRepository;

@Repository
public class JabatanDaoImpl implements JabatanDao{

	@Autowired
	private JabatanRepository jabatanRepository;
	
	@Override
	public Jabatan save(Jabatan t) {
		return jabatanRepository.save(t);
	}

	@Override
	public Jabatan edit(Jabatan t) {
		return jabatanRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		Jabatan obj = jabatanRepository.findById(id).get();
		jabatanRepository.delete(obj);
	}

	@Override
	public Jabatan findById(Long id) {
		return jabatanRepository.findById(id).get();
	}

	@Override
	public Page<JabatanDto> pagination(Pageable pageable) {
		return jabatanRepository.pagination(pageable);
	}

	@Override
	public Page<JabatanDto> pagination(Pageable pageable, String search) {
		return jabatanRepository.pagination(pageable, search);
	}

	@Override
	public Optional<JabatanDto> dtoFindById(Long id) {
		return jabatanRepository.dtoFindById(id);
	}

}
