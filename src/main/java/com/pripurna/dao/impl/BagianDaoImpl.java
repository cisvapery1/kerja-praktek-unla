package com.pripurna.dao.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.pripurna.dao.BagianDao;
import com.pripurna.dto.BagianDto;
import com.pripurna.model.Bagian;
import com.pripurna.repository.BagianRepository;

@Repository
public class BagianDaoImpl implements BagianDao{

	@Autowired
	private BagianRepository bagianRepository;
	
	@Override
	public Bagian save(Bagian t) {
		return bagianRepository.save(t);
	}

	@Override
	public Bagian edit(Bagian t) {
		return bagianRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		Bagian obj = bagianRepository.findById(id).get();
		bagianRepository.delete(obj);
	}

	@Override
	public Bagian findById(Long id) {
		return bagianRepository.findById(id).get();
	}

	@Override
	public Page<BagianDto> pagination(Pageable pageable) {
		return bagianRepository.pagination(pageable);
	}

	@Override
	public Page<BagianDto> pagination(Pageable pageable, String search) {
		return bagianRepository.pagination(pageable, search);
	}

	@Override
	public Optional<BagianDto> dtoFindById(Long id) {
		// TODO Auto-generated method stub
		return bagianRepository.dtoFindById(id);
	}

}
