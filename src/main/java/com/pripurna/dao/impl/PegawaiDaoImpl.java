package com.pripurna.dao.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.pripurna.dao.PegawaiDao;
import com.pripurna.dto.PegawaiDto;
import com.pripurna.model.Pegawai;
import com.pripurna.repository.PegawaiRepository;

@Repository
public class PegawaiDaoImpl implements PegawaiDao{

	@Autowired
	private PegawaiRepository pegawaiRepository;
	
	@Override
	public Pegawai save(Pegawai t) {
		return pegawaiRepository.save(t);
	}

	@Override
	public Pegawai edit(Pegawai t) {
		return pegawaiRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		Pegawai obj = pegawaiRepository.findById(id).get();
		pegawaiRepository.delete(obj);
	}

	@Override
	public Pegawai findById(Long id) {
		return pegawaiRepository.findById(id).get();
	}

	@Override
	public Page<PegawaiDto> pagination(Pageable pageable) {
		return pegawaiRepository.pagination(pageable);
	}

	@Override
	public Page<PegawaiDto> pagination(Pageable pageable, String search) {
		return pegawaiRepository.pagination(pageable, search);
	}

	@Override
	public Optional<PegawaiDto> dtoFindById(Long id) {
		return pegawaiRepository.dtoFindById(id);
	}

}
