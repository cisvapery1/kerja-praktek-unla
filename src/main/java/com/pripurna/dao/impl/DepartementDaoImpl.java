package com.pripurna.dao.impl;

import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.pripurna.dao.DepartementDao;
import com.pripurna.dto.DepartementDto;
import com.pripurna.model.Departement;
import com.pripurna.repository.DepartementRepository;

@Repository
public class DepartementDaoImpl implements DepartementDao{

	@Autowired
	private DepartementRepository departementRepository;
	
	@Override
	public Departement save(Departement t) {
		return departementRepository.save(t);
	}

	@Override
	public Departement edit(Departement t) {
		return departementRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		Departement obj = departementRepository.findById(id).get();
		departementRepository.delete(obj);
	}

	@Override
	public Departement findById(Long id) {
		return departementRepository.findById(id).get();
	}

	@Override
	public Page<DepartementDto> pagination(Pageable pageable) {
		return departementRepository.pagination(pageable);
	}

	@Override
	public Page<DepartementDto> pagination(Pageable pageable, String search) {
		return departementRepository.pagination(pageable, search);
	}

	@Override
	public Optional<DepartementDto> dtoFindById(Long id) {
		return departementRepository.dtoFindById(id);
	}

}
