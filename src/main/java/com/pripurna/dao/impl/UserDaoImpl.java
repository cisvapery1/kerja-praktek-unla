package com.pripurna.dao.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.pripurna.dao.UserDao;
import com.pripurna.dto.UserDto;
import com.pripurna.model.User;
import com.pripurna.repository.UserRepository;

@Repository
public class UserDaoImpl implements UserDao{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User save(User t) {
		return userRepository.save(t);
	}

	@Override
	public User edit(User t) {
		return userRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		User obj = userRepository.findById(id).get();
		userRepository.delete(obj);
	}

	@Override
	public User findById(Long id) {
		return userRepository.findById(id).get();
	}

	@Override
	public Page<UserDto> pagination(Pageable pageable) {
		return userRepository.pagination(pageable);
	}

	@Override
	public Page<UserDto> pagination(Pageable pageable, String search) {
		return userRepository.pagination(pageable, search);
	}

	@Override
	public Optional<UserDto> dtoFindById(Long id) {
		// TODO Auto-generated method stub
		return userRepository.dtoFindById(id);
	}

}
