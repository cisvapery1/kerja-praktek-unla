package com.pripurna.dao.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.pripurna.dao.PegawaiCutiDao;
import com.pripurna.dto.PegawaiCutiDto;
import com.pripurna.model.PegawaiCuti;
import com.pripurna.repository.PegawaiCutiRepository;

@Repository
public class PegawaiCutiDaoImpl implements PegawaiCutiDao{

	@Autowired
	private PegawaiCutiRepository pegawaiCutiRepository;
	
	@Override
	public PegawaiCuti save(PegawaiCuti t) {
		return pegawaiCutiRepository.save(t);
	}

	@Override
	public PegawaiCuti edit(PegawaiCuti t) {
		return pegawaiCutiRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		PegawaiCuti obj = pegawaiCutiRepository.findById(id).get();
		pegawaiCutiRepository.delete(obj);
	}

	@Override
	public PegawaiCuti findById(Long id) {
		return pegawaiCutiRepository.findById(id).get();
	}

	@Override
	public Page<PegawaiCutiDto> pagination(Pageable pageable) {
		return pegawaiCutiRepository.pagination(pageable);
	}

	@Override
	public Page<PegawaiCutiDto> pagination(Pageable pageable, String search) {
		return pegawaiCutiRepository.pagination(pageable, search);
	}

	@Override
	public Optional<PegawaiCutiDto> dtoFindById(Long id) {
		return pegawaiCutiRepository.dtoFindById(id);
	}

}
