package com.pripurna.dao.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.pripurna.dao.CutiDao;
import com.pripurna.dto.CutiDto;
import com.pripurna.model.Cuti;
import com.pripurna.repository.CutiRepository;

@Repository
public class CutiDaoImpl implements CutiDao{

	@Autowired
	private CutiRepository cutiRepository; 
	
	@Override
	public Cuti save(Cuti t) {
		return cutiRepository.save(t);
	}

	@Override
	public Cuti edit(Cuti t) {
		return cutiRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		Cuti cuti = cutiRepository.findById(id).get();
		cutiRepository.delete(cuti);
	}

	@Override
	public Cuti findById(Long id) {
		return cutiRepository.findById(id).get();
	}

	@Override
	public Page<CutiDto> pagination(Pageable pageable) {
		return cutiRepository.pagination(pageable);
	}

	@Override
	public Page<CutiDto> pagination(Pageable pageable, String search) {
		return cutiRepository.pagination(pageable, search);
	}

	@Override
	public Optional<CutiDto> dtoFindById(Long id) {
		return cutiRepository.dtoFindById(id);
	}

}
