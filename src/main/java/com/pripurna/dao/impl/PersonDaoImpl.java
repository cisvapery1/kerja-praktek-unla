package com.pripurna.dao.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.pripurna.dao.PersonDao;
import com.pripurna.dto.PersonDto;
import com.pripurna.model.Person;
import com.pripurna.repository.PersonRepository;

@Repository
public class PersonDaoImpl implements PersonDao{

	@Autowired
	private PersonRepository personRepository;
	
	@Override
	public Person save(Person t) {
		return personRepository.save(t);
	}

	@Override
	public Person edit(Person t) {
		return personRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		Person obj = personRepository.findById(id).get();
		personRepository.delete(obj);
	}

	@Override
	public Person findById(Long id) {
		return personRepository.findById(id).get();
	}

	@Override
	public Page<PersonDto> pagination(Pageable pageable) {
		return personRepository.pagination(pageable);
	}

	@Override
	public Page<PersonDto> pagination(Pageable pageable, String search) {
		return personRepository.pagination(pageable, search);
	}

	@Override
	public Optional<PersonDto> dtoFindById(Long id) {
		return personRepository.dtoFindById(id);
	}

}
