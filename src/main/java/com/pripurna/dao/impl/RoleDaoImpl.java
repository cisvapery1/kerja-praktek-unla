package com.pripurna.dao.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.pripurna.dao.RoleDao;
import com.pripurna.dto.RoleDto;
import com.pripurna.model.Role;
import com.pripurna.repository.RoleRepository;

@Repository
public class RoleDaoImpl implements RoleDao{

	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public Role save(Role t) {
		return roleRepository.save(t);
	}

	@Override
	public Role edit(Role t) {
		return roleRepository.save(t);
	}

	@Override
	public void delete(Long id) {
		Role obj = roleRepository.findById(id).get();
		roleRepository.delete(obj);
	}

	@Override
	public Role findById(Long id) {
		return roleRepository.findById(id).get();
	}

	@Override
	public Page<RoleDto> pagination(Pageable pageable) {
		return roleRepository.pagination(pageable);
	}

	@Override
	public Page<RoleDto> pagination(Pageable pageable, String search) {
		return roleRepository.pagination(pageable, search);
	}

	@Override
	public Optional<RoleDto> dtoFindById(Long id) {
		// TODO Auto-generated method stub
		return roleRepository.dtoFindById(id);
	}

}
