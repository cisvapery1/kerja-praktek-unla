package com.pripurna.dao;

import com.pripurna.dto.CutiDto;
import com.pripurna.model.Cuti;

public interface CutiDao extends CrudDao<Cuti>, PaginationDao<CutiDto>, ModelDto<CutiDto>{
}