package com.pripurna.dao;

import com.pripurna.dto.RoleDto;
import com.pripurna.model.Role;

public interface RoleDao extends CrudDao<Role>, PaginationDao<RoleDto>, ModelDto<RoleDto>{

}