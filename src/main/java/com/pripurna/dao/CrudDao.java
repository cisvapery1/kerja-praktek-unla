package com.pripurna.dao;

public interface CrudDao<T> {

	public T save(T t);
	public T edit(T t);
	public void delete(Long id);
	public T findById(Long id);
	
}
