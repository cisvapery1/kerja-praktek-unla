package com.pripurna.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PaginationDao<T> {

	public Page<T> pagination(Pageable pageable);
	public Page<T> pagination(Pageable pageable, String search);
	
}
