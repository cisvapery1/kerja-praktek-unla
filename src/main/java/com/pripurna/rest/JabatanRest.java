package com.pripurna.rest;

import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.pripurna.dto.JabatanDto;
import com.pripurna.repository.JabatanRepository;
import com.pripurna.service.JabatanService;
import com.pripurna.util.ModelDataTable;

@RestController
@RequestMapping("/rest/jabatan")
public class JabatanRest {

	@Autowired
	private JabatanService jabatanService;
	
	@Autowired
	private JabatanRepository jabatanRepository;

	@GetMapping
	public ModelDataTable<JabatanDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {
		return jabatanService.pagination(pageSize, page, search, sortBy, sortDirection);
	}
	
//	@GetMapping
//	public ResponseEntity<?> pagination(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
//			Optional<String> sortBy, Optional<String> sortDirection) {
//		return new ResponseEntity<ModelDataTable<JabatanDto>>(
//				jabatanService.pagination(pageSize, page, search, sortBy, sortDirection), HttpStatus.OK);
//	}

	@GetMapping("/report")
	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {
		return jabatanService.report(pageSize, page, search, sortBy, sortDirection, model, req, res, principal,
				session);
	}
	
	@GetMapping("/show/{id}")
	public ResponseEntity<?> dtoFindById(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<JabatanDto>(jabatanRepository.dtoFindById(id).get(), HttpStatus.OK);
	}

	@GetMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {
		try {
			jabatanRepository.delete(jabatanRepository.findById(id).get());
			return new ResponseEntity<String>("success", HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<String>("error", HttpStatus.BAD_REQUEST);
		}
		
	}
}
