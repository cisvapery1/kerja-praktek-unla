package com.pripurna.rest;

import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.pripurna.dto.UserDto;
import com.pripurna.service.UserService;
import com.pripurna.util.ModelDataTable;

@RestController
@RequestMapping("/rest/user")
public class UserRest {

	@Autowired
	private UserService userService;

	@GetMapping
	public ModelDataTable<UserDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {
		return userService.pagination(pageSize, page, search, sortBy, sortDirection);
	}

	@GetMapping("/report")
	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {
		return userService.report(pageSize, page, search, sortBy, sortDirection, model, req, res, principal,
				session);
	}
	
}