package com.pripurna.rest;

import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.pripurna.dto.PegawaiDto;
import com.pripurna.repository.PegawaiRepository;
import com.pripurna.service.PegawaiService;
import com.pripurna.util.ModelDataTable;

@RestController
@RequestMapping("/rest/pegawai")
public class PegawaiRest {

	@Autowired
	private PegawaiService pegawaiService;
	
	@Autowired
	private PegawaiRepository pegawaiRepository;

	@GetMapping
	public ModelDataTable<PegawaiDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {
		return pegawaiService.pagination(pageSize, page, search, sortBy, sortDirection);
	}

	@GetMapping("/report")
	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {
		return pegawaiService.report(pageSize, page, search, sortBy, sortDirection, model, req, res, principal,
				session);
	}
	
	@GetMapping("/show/{id}")
	public ResponseEntity<?> dtoFindById(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<PegawaiDto>(pegawaiRepository.dtoFindById(id).get(), HttpStatus.OK);
	}

	@GetMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {
		try {
			pegawaiRepository.delete(pegawaiRepository.findById(id).get());
			return new ResponseEntity<String>("success", HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<String>("error", HttpStatus.BAD_REQUEST);
		}
		
	}
	
}
