package com.pripurna.rest;

import java.security.Principal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.pripurna.dto.PersonDto;
import com.pripurna.service.PersonService;
import com.pripurna.util.ModelDataTable;

@RestController
@RequestMapping("/rest/person")
public class PersonRest {

	@Autowired
	private PersonService personService;

	@GetMapping
	public ModelDataTable<PersonDto> pagination(Optional<Integer> pageSize, Optional<Integer> page,
			Optional<String> search, Optional<String> sortBy, Optional<String> sortDirection) {
		return personService.pagination(pageSize, page, search, sortBy, sortDirection);
	}

	@GetMapping("/report")
	public ModelAndView report(Optional<Integer> pageSize, Optional<Integer> page, Optional<String> search,
			Optional<String> sortBy, Optional<String> sortDirection, Model model, HttpServletRequest req,
			HttpServletResponse res, Principal principal, HttpSession session) {
		return personService.report(pageSize, page, search, sortBy, sortDirection, model, req, res, principal,
				session);
	}
	
}
