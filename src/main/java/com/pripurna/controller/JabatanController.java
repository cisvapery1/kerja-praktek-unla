package com.pripurna.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pripurna.form.JabatanForm;
import com.pripurna.model.Bagian;
import com.pripurna.model.Cuti;
import com.pripurna.model.Departement;
import com.pripurna.model.Jabatan;
import com.pripurna.repository.CutiRepository;
import com.pripurna.repository.JabatanRepository;

@Controller
@RequestMapping("/jabatan")
public class JabatanController {

	@Autowired
	private JabatanRepository jabatanRepository;
	
	@Autowired
	private CutiRepository cutiRepository;

	@GetMapping
	public String index(Model model) {
		return "jabatan/index";
	}

	@GetMapping("/add")
	public String add(JabatanForm jabatanForm, Model model) {
		model.addAttribute("cutis", cutiRepository.findAll());
		return "jabatan/add";
	}

	@PostMapping("/add")
	public String addAction(@Valid JabatanForm jabatanForm, Model model, BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute("cutis", cutiRepository.findAll());
			return "jabatan/add";
		}
		Long cutiId = jabatanForm.getCutiId();
		String name = jabatanForm.getName();
		String deskripsi = jabatanForm.getDeskripsi();

		Cuti cuti = cutiRepository.findById(cutiId).get();
		Jabatan jabatan = new Jabatan();
		jabatan.setName(name);
		jabatan.setDeskripsi(deskripsi);
		jabatan.setCuti(cuti);
		jabatanRepository.save(jabatan);

		model.addAttribute("message", "Data berhasil di simpan.");
		return "jabatan/index";
	}

	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, JabatanForm jabatanForm, Model model) {
		if (!jabatanRepository.findById(id).isPresent()) {
			model.addAttribute("message", "Data tidak di temukan");
			return "jabatan/index";
		}
		model.addAttribute("cutis", cutiRepository.findAll());

		Jabatan jabatan = jabatanRepository.findById(id).get();
		jabatanForm.setName(jabatan.getName());
		jabatanForm.setDeskripsi(jabatan.getDeskripsi());
		jabatanForm.setCutiId(jabatan.getCuti().getId());

		return "jabatan/edit";
	}

	@PostMapping("/edit/{id}")
	public String editAction(@PathVariable("id") Long id, @Valid JabatanForm jabatanForm, Model model,
			BindingResult result) {
		
		if (result.hasErrors()) {
			model.addAttribute("message", "Data tidak di temukan");
			model.addAttribute("cutis", cutiRepository.findAll());
			Jabatan jabatan = jabatanRepository.findById(id).get();
			jabatanForm.setName(jabatan.getName());
			jabatanForm.setDeskripsi(jabatan.getDeskripsi());
			jabatanForm.setCutiId(jabatan.getCuti().getId());
			return "jabatan/edit";
			
		}
		Long cutiId = jabatanForm.getCutiId();
		String name = jabatanForm.getName();
		String deskripsi = jabatanForm.getDeskripsi();

		Cuti cuti = cutiRepository.findById(cutiId).get();
		Jabatan jabatan = jabatanRepository.findById(id).get();
		jabatan.setName(name);
		jabatan.setDeskripsi(deskripsi);
		jabatan.setCuti(cuti);
		jabatanRepository.save(jabatan);

		model.addAttribute("message", "Data berhasil di edit.");
		return "jabatan/index";
	}
}
