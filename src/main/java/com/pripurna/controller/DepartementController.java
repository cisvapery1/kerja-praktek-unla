package com.pripurna.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pripurna.form.DepartementForm;
import com.pripurna.model.Departement;
import com.pripurna.repository.BagianRepository;
import com.pripurna.repository.DepartementRepository;

@Controller
@RequestMapping("/departement")
public class DepartementController {

	@Autowired
	private DepartementRepository departementRepository;

	@Autowired
	private BagianRepository bagianRepository;

	@GetMapping
	public String index(Model model) {
		return "departement/index";
	}

	@GetMapping("/add")
	public String add(DepartementForm departementForm, Model model) {
		return "departement/add";
	}

	@PostMapping("/add")
	public String addAction(@Valid DepartementForm departementForm, Model model, BindingResult result) {
		if (result.hasErrors()) {
			return "departement/add";
		}
		String name = departementForm.getName();
		String deskripsi = departementForm.getDeskripsi();

		Departement obj = new Departement();
		obj.setName(name);
		obj.setDeskripsi(deskripsi);
		departementRepository.save(obj);

		model.addAttribute("message", "Data berhasil di simpan.");
		return "departement/index";
	}

	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, DepartementForm departementForm, Model model) {
		if (!bagianRepository.findById(id).isPresent()) {
			return "departement/index";
		}

		Departement obj = departementRepository.findById(id).get();
		departementForm.setName(obj.getName());
		departementForm.setDeskripsi(obj.getDeskripsi());

		return "departement/edit";
	}

	@PostMapping("/edit/{id}")
	public String editAction(@PathVariable("id") Long id, @Valid DepartementForm departementForm, Model model,
			BindingResult result) {
		
		if (result.hasErrors()) {
			Departement obj = departementRepository.findById(id).get();
			departementForm.setName(obj.getName());
			departementForm.setDeskripsi(obj.getDeskripsi());
			return "departement/edit";
			
		}
		String name = departementForm.getName();
		String deskripsi = departementForm.getDeskripsi();

		Departement obj = departementRepository.findById(id).get();
		obj.setName(name);
		obj.setDeskripsi(deskripsi);
		departementRepository.save(obj);

		model.addAttribute("message", "Data berhasil di edit.");
		return "departement/index";
	}
	
}
