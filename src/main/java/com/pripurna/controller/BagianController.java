package com.pripurna.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pripurna.form.BagianForm;
import com.pripurna.model.Bagian;
import com.pripurna.model.Departement;
import com.pripurna.repository.BagianRepository;
import com.pripurna.repository.DepartementRepository;
import com.pripurna.service.BagianService;

@Controller
@RequestMapping("/bagian")
public class BagianController {

	@Autowired
	private DepartementRepository departementRepository;

	@Autowired
	private BagianService bagianService;

	@Autowired
	private BagianRepository bagianRepository;

	@GetMapping
	public String index(Model model) {
		return "bagian/index";
	}

	@GetMapping("/add")
	public String add(BagianForm bagianForm, Model model) {
		model.addAttribute("departements", departementRepository.findAll());
		return "bagian/add";
	}

	@PostMapping("/add")
	public String addAction(@Valid BagianForm bagianForm, Model model, BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute("departements", departementRepository.findAll());
			return "bagian/add";
		}
		Long departementId = bagianForm.getDepartementId();
		String name = bagianForm.getName();
		String deskripsi = bagianForm.getDeskripsi();

		Departement departement = departementRepository.findById(departementId).get();
		Bagian bagian = new Bagian();
		bagian.setName(name);
		bagian.setDeskripsi(deskripsi);
		bagian.setDepartement(departement);
		bagianRepository.save(bagian);

		model.addAttribute("message", "Data berhasil di simpan.");
		return "bagian/index";
	}

	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, BagianForm bagianForm, Model model) {
		if (!bagianRepository.findById(id).isPresent()) {
			return "bagian/index";
		}
		model.addAttribute("departements", departementRepository.findAll());

		Bagian bagian = bagianRepository.findById(id).get();
		bagianForm.setName(bagian.getName());
		bagianForm.setDeskripsi(bagian.getDeskripsi());
		bagianForm.setDepartementId(bagian.getDepartement().getId());

		return "bagian/edit";
	}

	@PostMapping("/edit/{id}")
	public String editAction(@PathVariable("id") Long id, @Valid BagianForm bagianForm, Model model,
			BindingResult result) {
		
		if (result.hasErrors()) {
			model.addAttribute("departements", departementRepository.findAll());

			Bagian bagian = bagianRepository.findById(id).get();
			bagianForm.setName(bagian.getName());
			bagianForm.setDeskripsi(bagian.getDeskripsi());
			bagianForm.setDepartementId(bagian.getDepartement().getId());
			return "bagian/edit";
			
		}
		Long departementId = bagianForm.getDepartementId();
		String name = bagianForm.getName();
		String deskripsi = bagianForm.getDeskripsi();

		Departement departement = departementRepository.findById(departementId).get();
		Bagian bagian = bagianRepository.findById(id).get();
		bagian.setName(name);
		bagian.setDeskripsi(deskripsi);
		bagian.setDepartement(departement);
		bagianService.save(bagian);

		model.addAttribute("message", "Data berhasil di edit.");
		return "bagian/index";
	}

}