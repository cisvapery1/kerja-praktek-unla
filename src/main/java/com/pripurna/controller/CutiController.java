package com.pripurna.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pripurna.form.BagianForm;
import com.pripurna.form.CutiForm;
import com.pripurna.model.Bagian;
import com.pripurna.model.Cuti;
import com.pripurna.model.Departement;
import com.pripurna.repository.CutiRepository;

@Controller
@RequestMapping("/cuti")
public class CutiController {

	@Autowired
	private CutiRepository cutiRepository;

	@GetMapping
	public String index(Model model) {
		return "cuti/index";
	}

	@GetMapping("/add")
	public String add(CutiForm cutiForm, Model model) {
		return "cuti/add";
	}

	@PostMapping("/add")
	public String addAction(@Valid CutiForm cutiForm, Model model, BindingResult result) {
		if (result.hasErrors()) {
			return "cuti/add";
		}
		Integer jumlahCutiTahunan = cutiForm.getJumlahCutiTahunan();
		String type = cutiForm.getType();

		Cuti cuti =new Cuti();
		cuti.setJumlahCutiTahunan(jumlahCutiTahunan);
		cuti.setType(type);
		cutiRepository.save(cuti);

		model.addAttribute("message", "Data berhasil di simpan.");
		return "cuti/index";
	}

	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, CutiForm cutiForm, Model model) {
		if (!cutiRepository.findById(id).isPresent()) {
			return "cuti/index";
		}

		Cuti cuti = cutiRepository.findById(id).get();
		cutiForm.setJumlahCutiTahunan(cuti.getJumlahCutiTahunan());
		cutiForm.setId(cuti.getId());
		cutiForm.setType(cuti.getType());

		return "cuti/edit";
	}

	@PostMapping("/edit/{id}")
	public String editAction(@PathVariable("id") Long id, @Valid CutiForm cutiForm, Model model,
			BindingResult result) {
		
		if (result.hasErrors()) {
			Cuti cuti = cutiRepository.findById(id).get();
			cutiForm.setJumlahCutiTahunan(cuti.getJumlahCutiTahunan());
			cutiForm.setId(cuti.getId());
			cutiForm.setType(cuti.getType());
			return "cuti/edit";
			
		}
		Integer jumlahCutiTahunan = cutiForm.getJumlahCutiTahunan();
		String type = cutiForm.getType();

		Cuti cuti = cutiRepository.findById(id).get();
		cuti.setJumlahCutiTahunan(jumlahCutiTahunan);
		cuti.setType(type);
		cutiRepository.save(cuti);

		model.addAttribute("message", "Data berhasil di edit.");
		return "cuti/index";
	}
	
}