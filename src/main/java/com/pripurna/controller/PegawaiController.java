package com.pripurna.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pripurna.form.PegawaiForm;
import com.pripurna.model.Bagian;
import com.pripurna.model.Departement;
import com.pripurna.model.Pegawai;
import com.pripurna.repository.BagianRepository;
import com.pripurna.repository.JabatanRepository;
import com.pripurna.repository.PegawaiRepository;
import com.pripurna.repository.PersonRepository;

@Controller
@RequestMapping("/pegawai")
public class PegawaiController {
	
	@Autowired
	private PegawaiRepository pegawaiRepository;
	
	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
	private JabatanRepository jabatanRepository;
	
	@Autowired
	private BagianRepository bagianRepository;

	@GetMapping
	public String index(Model model) {
		return "pegawai/index";
	}

	@GetMapping("/add")
	public String add(PegawaiForm pegawaiForm, Model model) {
		model.addAttribute("persons", personRepository.findAll());
		model.addAttribute("jabatans", jabatanRepository.findAll());
		model.addAttribute("bagians", bagianRepository.findAll());
		return "pegawai/add";
	}

	@PostMapping("/add")
	public String addAction(@Valid PegawaiForm pegawaiForm, Model model, BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute("persons", personRepository.findAll());
			model.addAttribute("jabatans", jabatanRepository.findAll());
			model.addAttribute("bagians", bagianRepository.findAll());
			return "pegawai/add";
		}
		
		Long bagianId = pegawaiForm.getBagianId();
		Long jabatanId = pegawaiForm.getJabatanId();
		Long personId = pegawaiForm.getPersonId();
		
		Long id = pegawaiForm.getId();
		String npwp = pegawaiForm.getNpwp();
		String pendidikanTerakhir = pegawaiForm.getPendidikanTerakhir();
		String statusPekerjaan = pegawaiForm.getStatusPekerjaan();
		Date tanggalMasuk = pegawaiForm.getTanggalMasuk();
		Integer limitCutiTahunan = pegawaiForm.getLimitCutiTahunan();

		Pegawai pegawai = new Pegawai();
		pegawai.setBagian(bagianRepository.findById(bagianId).get());
		pegawai.setJabatan(jabatanRepository.findById(jabatanId).get());
		pegawai.setLimitCutiTahunan(limitCutiTahunan);
		pegawai.setNpwp(npwp);
		pegawai.setPendidikanTerakhir(pendidikanTerakhir);
		pegawai.setPerson(personRepository.findById(personId).get());
		pegawai.setStatusPekerjaan(statusPekerjaan);
		pegawai.setTanggalMasuk(tanggalMasuk);
		pegawaiRepository.save(pegawai);
		
		model.addAttribute("message", "Data berhasil di simpan.");
		return "pegawai/index";
	}

	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, PegawaiForm pegawaiForm, Model model) {
		if (!bagianRepository.findById(id).isPresent()) {
			return "pegawai/index";
		}
		model.addAttribute("persons", personRepository.findAll());
		model.addAttribute("jabatans", jabatanRepository.findAll());
		model.addAttribute("bagians", bagianRepository.findAll());

		Pegawai pegawai = pegawaiRepository.findById(id).get();
		pegawaiForm.setBagianId(pegawai.getBagian().getId());
		pegawaiForm.setJabatanId(pegawai.getJabatan().getId());
		pegawaiForm.setLimitCutiTahunan(pegawai.getLimitCutiTahunan());
		pegawaiForm.setNpwp(pegawai.getNpwp());
		pegawaiForm.setPendidikanTerakhir(pegawai.getPendidikanTerakhir());
		pegawaiForm.setPersonId(pegawai.getPerson().getId());
		pegawaiForm.setStatusPekerjaan(pegawai.getStatusPekerjaan());
		pegawaiForm.setTanggalMasuk(pegawai.getTanggalMasuk());
		pegawaiForm.setId(pegawai.getId());

		return "pegawai/edit";
	}

	@PostMapping("/edit/{id}")
	public String editAction(@PathVariable("id") Long id, @Valid PegawaiForm pegawaiForm, Model model,
			BindingResult result) {
		
		if (result.hasErrors()) {
			model.addAttribute("persons", personRepository.findAll());
			model.addAttribute("jabatans", jabatanRepository.findAll());
			model.addAttribute("bagians", bagianRepository.findAll());

			Pegawai pegawai = pegawaiRepository.findById(id).get();
			pegawaiForm.setBagianId(pegawai.getBagian().getId());
			pegawaiForm.setJabatanId(pegawai.getJabatan().getId());
			pegawaiForm.setLimitCutiTahunan(pegawai.getLimitCutiTahunan());
			pegawaiForm.setNpwp(pegawai.getNpwp());
			pegawaiForm.setPendidikanTerakhir(pegawai.getPendidikanTerakhir());
			pegawaiForm.setPersonId(pegawai.getPerson().getId());
			pegawaiForm.setStatusPekerjaan(pegawai.getStatusPekerjaan());
			pegawaiForm.setTanggalMasuk(pegawai.getTanggalMasuk());
			pegawaiForm.setId(pegawai.getId());
			return "pegawai/edit";
			
		}
		Long bagianId = pegawaiForm.getBagianId();
		Long jabatanId = pegawaiForm.getJabatanId();
		Long personId = pegawaiForm.getPersonId();
		
		String npwp = pegawaiForm.getNpwp();
		String pendidikanTerakhir = pegawaiForm.getPendidikanTerakhir();
		String statusPekerjaan = pegawaiForm.getStatusPekerjaan();
		Date tanggalMasuk = pegawaiForm.getTanggalMasuk();
		Integer limitCutiTahunan = pegawaiForm.getLimitCutiTahunan();

		Pegawai pegawai = pegawaiRepository.findById(id).get();
		pegawai.setBagian(bagianRepository.findById(bagianId).get());
		pegawai.setJabatan(jabatanRepository.findById(jabatanId).get());
		pegawai.setLimitCutiTahunan(limitCutiTahunan);
		pegawai.setNpwp(npwp);
		pegawai.setPendidikanTerakhir(pendidikanTerakhir);
		pegawai.setPerson(personRepository.findById(personId).get());
		pegawai.setStatusPekerjaan(statusPekerjaan);
		pegawai.setTanggalMasuk(tanggalMasuk);
		pegawaiRepository.save(pegawai);
		model.addAttribute("message", "Data berhasil di edit.");
		return "pegawai/index";
	}
}
