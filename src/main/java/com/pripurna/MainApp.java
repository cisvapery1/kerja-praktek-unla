package com.pripurna;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.pripurna.model.Bagian;
import com.pripurna.model.Cuti;
import com.pripurna.model.Departement;
import com.pripurna.model.Jabatan;
import com.pripurna.model.Pegawai;
import com.pripurna.model.Person;
import com.pripurna.model.Role;
import com.pripurna.model.User;
import com.pripurna.service.BagianService;
import com.pripurna.service.CutiService;
import com.pripurna.service.DepartementService;
import com.pripurna.service.JabatanService;
import com.pripurna.service.PegawaiCutiService;
import com.pripurna.service.PegawaiService;
import com.pripurna.service.PersonService;
import com.pripurna.service.RoleService;
import com.pripurna.service.UserService;

@SpringBootApplication
public class MainApp implements CommandLineRunner {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private PersonService personService;

	@Autowired
	private PegawaiService pegawaiService;
	
	@Autowired
	private DepartementService departementService;
	
	@Autowired
	private BagianService bagianService;
	
	@Autowired
	private JabatanService jabatanService;
	
	@Autowired
	private CutiService cutiService;
	
	@Autowired
	private PegawaiCutiService pegawaiCutiService;

	@Autowired
	private BCryptPasswordEncoder pass;

	public static void main(String[] args) {
		SpringApplication.run(MainApp.class, args);
	}

	@Transactional
	@Override
	public void run(String... args) throws Exception {

//		Person person = new Person();
//		person.setAgama("ISLAM");
//		person.setAlamat("Subang");
//		person.setName("Peri Purnama");
//		person.setKewarganegaraan("WNI");
//		person.setNik("41155055170197");
//		person.setStatusPerkawinan("Belum menikah");
//		person.setTanggalLahir(new Date());
//		person.setTelepon("082320795975");
//		
//		personService.save(person);
//
//		Role role = new Role();
//		role.setName("ROLE_ADMIN");
//		role.setDeskripsi("ROLE_ADMIN");
//		roleService.save(role);
//
//		User user = new User();
//		user.setEmail("peri.purnama@bts.id");
//		user.setPerson(person);
//		user.setPassword(pass.encode("admin"));
//		user.setUsername("admin");
//		user.addRole(role);
//		userService.save(user);
//
//		Departement department = new Departement();
//		department.setName("Developer");
//		department.setDeskripsi("Developer");
//		departementService.save(department);
//		
//		Bagian bagian = new Bagian();
//		bagian.setName("Proyek");
//		bagian.setDeskripsi("Proyek management");
//		bagian.setDepartement(department);
//		bagianService.save(bagian);
//		
//		Cuti cuti = new Cuti();
//		cuti.setJumlahCutiTahunan(12);
//		cuti.setType("Karyawan tetap");
//		cutiService.save(cuti);
//		
//		Jabatan jabatan = new Jabatan();
//		jabatan.setName("Developer Java");
//		jabatan.setDeskripsi("Developer Java");
//		jabatan.setCuti(cuti);
//		jabatanService.save(jabatan);
//		
//		Pegawai pegawai = new Pegawai();
//		pegawai.setBagian(bagian);
//		pegawai.setJabatan(jabatan);
//		pegawai.setPendidikanTerakhir("SMK");
//		pegawai.setPerson(person);
//		pegawai.setStatusPekerjaan("Aktif");
//		pegawai.setTanggalMasuk(new Date());
//		pegawaiService.save(pegawai);
		

	}

}