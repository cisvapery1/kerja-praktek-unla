$(document).ready(function () {
	
	pagination();
	changePageAndSize()
	
	$('#printXls').on('click', function(i) {
		var pageSizeSelected = $('#pageSizeSelect').children('option:selected').val();
		var page = $(".pagination li.active").find('a').attr('page');
		var search = $('#search').val();
    	if(pageSizeSelected == undefined) {
    		$.ajax({
            	method: "GET",
            	url: '/rest/cuti?pageSize=' + 5,
            	success: function(obj) {
            		var optionPageSize = "";
            		$.each(obj.pageSize, function (i, o){
            			optionPageSize += "<option value='"+o+"'>"+o+"</option>";
            		})
            		$('#pageSizeSelect').append(optionPageSize);
            	}
        	})
        	pageSizeSelected = 3;
    	}
    	if(page == undefined) {
    		page = 1;
    	}
		window.open("/rest/cuti/report?pageSize="+pageSizeSelected+"&page="+page+"&search="+search+"&type=xls");
	})
	
	$('#printPdf').on('click', function(i) {
		var pageSizeSelected = $('#pageSizeSelect').children('option:selected').val();
		var page = $(".pagination li.active").find('a').attr('page');
		var search = $('#search').val();
		
    	if(pageSizeSelected == undefined) {
    		$.ajax({
            	method: "GET",
            	url: '/rest/cuti?pageSize=' + 5,
            	success: function(obj) {
            		var optionPageSize = "";
            		$.each(obj.pageSize, function (i, o){
            			optionPageSize += "<option value='"+o+"'>"+o+"</option>";
            		})
            		$('#pageSizeSelect').append(optionPageSize);
            	}
        	})
        	pageSizeSelected = 3;
    	}
    	if(page == undefined) {
    		page = 1;
    	}
    	
		window.open("/rest/cuti/report?pageSize="+pageSizeSelected+"&page="+page+"&search="+search+"&type=pdf");
	})
	
	function info(){
		$(".view").on("click", function() {
			var id = $(this).attr("id");
			
			$(".title-info").html("Information Cuti");
			
			$("#table-info tbody").each(function() {
				$(this).children().remove();
			});
			$("#table-info tbody").fadeOut("slow");
			
			$.ajax({
				url : "/rest/cuti/show/" + id,
				method : "GET",
				success : function(obj) {
					
					if(obj.id == null) {
						location.reload();
					}
					
					var data = null;
					data += "<tr><td style='width:200px'>ID</td>";
					data += "<td>" + obj.id + "</td></tr>";
					
					data += "<tr><td style='width:200px'>Cuti Tahunan</td>";
					data += "<td>" + obj.jumlahCutiTahunan + "</td></tr>";
					
					data += "<tr><td style='width:200px'>Type</td>";
					data += "<td>" + obj.type + "</td></tr>";
					
					data += "<tr><td style='width:200px'>Tanggal di buat</td>";
					data += "<td>" + obj.formatCreatedDate + "</td></tr>";
					
					$('#table-info tbody').append(data);
					$("#table-info tbody").fadeIn("slow");
				}
			})
		})
	}
	
	
	
	// start changePageAndSize()
    function changePageAndSize() {
                
        $('#pageSizeSelect').change(function(){
        	pagination();
        })
        
    }
  	// end changePageAndSize()
    
    // start search
    $('#search').bind('change paste keyup', function() {
    	pagination();
    });
	// end search
    
    $('#btn-search').on('click', function() {
    	pagination();
    	$('.box').jmspinner(false);
    });
    
    $('#btn-refresh').on('click', function() {
    	$('#search').attr('value', '');
    	$('.pagination li').each(function() {
			$(this).removeClass('active');
		})
    	pagination();
    });
    
    // start actionPagination()
    function actionPagination() {
		$('.pagination li').each(function() {
	    	
	    	$(this).on({
	    		click: function() {
	    			$('.pagination li').each(function() {
	    				$(this).removeClass('active');
	    			})
	    			$(this).addClass('active');
	    			
	    			pagination();
	    	  	}
	    	})
	    });
    }
    // end actionPagination()
    
    
    // start pagination()
    function pagination() {
    	
    	var pageSizeSelected = $('#pageSizeSelect').children('option:selected').val();
    	
    	if(pageSizeSelected == undefined) {
    		$.ajax({
            	method: "GET",
            	url: '/rest/cuti?pageSize=' + 5,
            	success: function(obj) {
            		var optionPageSize = "";
            		$.each(obj.pageSize, function (i, o){
            			optionPageSize += "<option value='"+o+"'>"+o+"</option>";
            		})
            		$('#pageSizeSelect').append(optionPageSize);
            	}
        	})
        	pageSizeSelected = 3;
    	}
    	
    	$('#pageSizeSelect > option').each(function(i, o){
     		if($(this).val() == pageSizeSelected) {
     			$(this).prop("selected", true);
     		} 
     	})
    	 
        var search = $('#search').val();
    	var page = $(".pagination li.active").find('a').attr('page');
    	
    	if(page == undefined) {
    		var request = $.ajax({
            	method: "GET",
            	url: '/rest/cuti?pageSize=' + pageSizeSelected,
            	beforeSend: function() {
            		$("#table-data-server tbody")
                    .each(function (i) {
                         $(this).children().remove();
                    });
            		$('#table-data-server tbody').append("<td colspan='6'><div class='box'></div></td>");
            		$('.box').jmspinner('large');
                },
            	success: function(obj) {
            		console.log(obj)
            		var auth = obj.authName;
            		
            		var startPage = obj.pagerModel.startPage;
            		var endPage = obj.pagerModel.endPage;
            		
            		var size = obj.data.size;
            	    var totalPages = obj.data.totalPages;
            	    var totalElements = obj.data.totalElements;
            	    var number = obj.data.number;
            	    var numberOfElements = obj.data.numberOfElements;
            	    // start pagination
            		
            	    $(".pagination")
                    .each(function () {
                         $(this).children().remove();
                    });
            	    
            		if(totalElements != 0) {
            			if(number == 0) {
                			var li = "<li class='disabled'><a href='#' page='"+1+"'>&laquo;</a></li>";
                			$('.pagination').append(li);
                		} else {
                			var li = "<li><a href='#' page='"+1+"'>&laquo;</a></li>";
                			$('.pagination').append(li);
                		}
                		
                		if(number == 0) {
                			var li = "<li class='disabled'><a href='#' page='"+number+"'>&larr;</a></li>";
                			$('.pagination').append(li);
                		} else {
                			var li = "<li><a href='#' page='"+1+"'>&larr;</a></li>";
                			$('.pagination').append(li);
                		}
                		
                		// page = page - 1;
                		for(var i=startPage; i <= endPage; i++) {
                			var li = null;
                			
                			if(number == (i - 1)) {
                				li = "<li class='active'><a href='#' page='"+i+"'>"+i+"</a></li>";
                			} else {
                				li = "<li><a href='#' page='"+i+"'>"+i+"</a></li>";
                			}
                			$('.pagination').append(li);
                		}
                		
                		if((number+1) == totalPages) {
                			var li = "<li class='disabled'><a href='#' page='"+totalPages+"'>&rarr;</a></li>";
                			$('.pagination').append(li);
                		} else {
                			var li = "<li><a href='#' page='"+(number+2)+"'>&rarr;</a></li>";
                			$('.pagination').append(li);
                		}
                		
                		if((number+1) == totalPages) {
                			var li = "<li class='disabled'><a href='#' page='"+totalPages+"'>&raquo;</a></li>";
                			$('.pagination').append(li);
                		} else {
                			var li = "<li><a href='#' page='"+totalPages+"'>&raquo;</a></li>";
                			$('.pagination').append(li);
                		}
            		}
            		$("#table-data-server tbody")
                    .each(function (i) {
                         $(this).children().remove();
                    });
            	    
            	    var to = (number + 1) * size;
    		        if (to > totalElements) {
    		            to = totalElements;
    		        }
    		        
    		        var totalRow = obj.totalRow;
			        
				    if(search !== null && search !== '') {
			        	if(totalElements == 0) {
			        		$('#messagetable').text("Showing " + (0) + " to " + (to) + " of " + totalElements + " entries. (filtered from " + totalRow + " total entries)");
			        	} else {
			        		$('#messagetable').text("Showing " + ((number * size) + 1) + " to " + (to) + " of " + totalElements + " entries. (filtered from " + totalRow + " total entries)");
			        	}
			        	
			        } else {
			        	if(totalElements == 0) {
			        		$('#messagetable').text("Showing " + 0 + " to " + (to) + " of " + totalElements + " entries.");
			        	} else {
			        		$('#messagetable').text("Showing " + ((number * size) + 1) + " to " + (to) + " of " + totalElements + " entries.");
			        	}
			        	
			        }
				    if(obj.data.content.length == 0) {
				    	 var customerRow = "<tr class='no-data'><td colspan='5'><p>No data</p></td></tr>";
				    	 $('#table-data-server tbody').append(customerRow);
				    }
    	            $.each(obj.data.content, function (i, o) {
    	            	
    	            	var action =  '<td class="text-nowrap"> '
    	            		+'<a class="view" style="cursor: pointer;" data-toggle="modal" data-target=".myModal2" id="' + o.id + '"> <i class="fa fa-info text-inverse m-r-10" data-toggle="tooltip" data-id="' + o.id + '" data-placement="top" title="View" data-original-title="Tooltip top"></i></a>'
    	            		+'<a href="/cuti/edit/'+o.id+'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>'
    	            		+'<a href="#" data-toggle="tooltip" data-original-title="Close" class="delete" idx="'+o.id+'"> <i class="fa fa-close text-danger"></i> </a></td>';
    	            	var createdDate = '<span class="text-muted"><i class="fa fa-clock-o"></i> '+((o.createdDate == null) ? "" : o.formatCreatedDate)+'</span>';
    	            	var customerRow = '<tr>'
    	            		+ '<td>'
    	                    + ((number * size) + (i + 1))
    	                    + '</td>'
                            + '<td>'
                            + o.jumlahCutiTahunan
                            + '</td>'
                            + '<td>'
                            + o.type
                            + '</td>'
                            + '<td>'
                            + createdDate
                            + '</td>'
                            + '<td>'
                            + action
                            + '</td>'
                            + '</tr>';
                        $('#table-data-server tbody').append(customerRow);
    	        	})
    	        	$('.box').jmspinner(false);
    	            info();
    	            $( "a.delete" ).click(function() {
    	        		var idx = $(this).attr("idx");
    	        		console.log(idx)
    	        		swal("Yakin akan di hapus?", {
    	        			  buttons: {
    	        			    cancel: "Batal",
    	        			    catch: {
    	        			      text: "Delete",
    	        			      value: "catch",
    	        			    },
    	        			  },
    	        			})
    	        			.then((value) => {
    	        			  switch (value) {
    	        			 
    	        			    case "catch":
	    	        			    $.ajax({
	                           			  methode: "GET",
	                           			  url: "/rest/cuti/delete/" + idx,
	                           			  success: function() {
	                           				  
	                           				 $("#table-data-server tbody")
	                                           .each(function () {
	                                               $(this).children().remove();
	                                           });
	                               			$("#table-data-server tbody").fadeOut("slow");
	                               			
	                           				 pagination();
	                           			  },
	                           			  statusCode: {
	  	                       				  400: function() {
	  	                       					 swal(
	  	                                    	           'Can not be deleted!',
	  	                                    	           'Sorry, this data cannot be deleted. This data has relations with other data.',
	  	                                    	           'warning'
	  	                                    	     )
	  	                       				  },
	  	                       				  200: function() {
	  	                       					 swal(
	  	                                      	           'Deleted!',
	  	                                      	           'Your file has been deleted.',
	  	                                      	           'success'
	  	                                      	     )
	  	                       				  }
	  	                       			  }
	                           		  })
	                           		  swal("Berhasil!", "Data berhasil dihapus!", "success");
    	        			      break;
    	        			 
    	        			  }
    	        			});
                 	}); 
            		actionPagination();
            	}
        	})
        	request.done(function( msg ) {
        		$('.box').jmspinner(false);
        	});
        		 
        	request.fail(function( jqXHR, textStatus ) {
        		$('.box').jmspinner(false);
        		$("#table-data-server tbody")
                .each(function (i) {
                     $(this).children().remove();
                });
        		var customerRow = "<tr class='no-data'><td colspan='6'><p>No data</p></td></tr>";
		    	 $('#table-data-server tbody').append(customerRow);
        	});
    	} else {
    		var request = $.ajax({
            	method: "GET",
            	url: '/rest/cuti?pageSize=' + pageSizeSelected + "&search=" + search,
            	beforeSend: function() {
                    // setting a timeout
            		$(".pagination")
                    .each(function () {
                         $(this).children().remove();
                    });
            		$("#table-data-server tbody")
                    .each(function (i) {
                         $(this).children().remove();
                    });
            		$('#table-data-server tbody').append("<td colspan='6'><div class='box'></div></td>");
            		$('.box').jmspinner('large');
                },
            	success: function(obj) {
            		var startPage = obj.pagerModel.startPage;
            		var endPage = obj.pagerModel.endPage;
            		
            		var size = obj.data.size;
            	    var totalPages = obj.data.totalPages;
            	    var totalElements = obj.data.totalElements;
            	    var number = obj.data.number;
            	    var numberOfElements = obj.data.numberOfElements;
            	    if(page > totalPages) {
            	    	page = totalPages;
                		
            	    }
            	   
            	    $.ajax({
                    	method: "GET",
                    	url: '/rest/cuti?pageSize=' + pageSizeSelected + "&page=" + page + '&search='+search,
                    	success: function(obj) {
                    		
                    		var auth = obj.authName;
                    		
                			$("#table-data-server tbody")
                            .each(function () {
                                 $(this).children().remove();
                            });
                			//$("#table-data-server tbody").fadeOut("slow");
                			
                			var startPage = obj.pagerModel.startPage;
                    		var endPage = obj.pagerModel.endPage;
                    		
                    		var size = obj.data.size;
                    	    var totalPages = obj.data.totalPages;
                    	    var totalElements = obj.data.totalElements;
                    	    var number = obj.data.number;
                    	    var numberOfElements = obj.data.numberOfElements;
                    	    
                    	    $(".pagination")
                            .each(function () {
                                 $(this).children().remove();
                            });
                    		
                    		// start pagination
                    		
                    		if(totalElements != 0) {
                    			if(number == 0) {
                        			var li = "<li class='disabled'><a href='#' page='"+1+"'>&laquo;</a></li>";
                        			$('.pagination').append(li);
                        		} else {
                        			var li = "<li><a href='#' page='"+1+"'>&laquo;</a></li>";
                        			$('.pagination').append(li);
                        		}
                        		
                        		if(number == 0) {
                        			var li = "<li class='disabled'><a href='#' page='"+1+"'>&larr;</a></li>";
                        			$('.pagination').append(li);
                        		} else {
                        			var li = "<li><a href='#' page='"+(page-1)+"'>&larr;</a></li>";
                        			$('.pagination').append(li);
                        		}
                        		
                        		// page = page - 1;
                        		for(var i=startPage; i <= endPage; i++) {
                        			var li = null;
                        			
                        			if(number == (i - 1)) {
                        				li = "<li class='active'><a href='#' page='"+i+"'>"+i+"</a></li>";
                        			} else {
                        				li = "<li><a href='#' page='"+i+"'>"+i+"</a></li>";
                        			}
                        			$('.pagination').append(li);
                        		}
                        		
                        		if((number+1) == totalPages) {
                        			var li = "<li class='disabled'><a href='#' page='"+totalPages+"'>&rarr;</a></li>";
                        			$('.pagination').append(li);
                        		} else {
                        			var li = "<li><a href='#' page='"+(number+2)+"'>&rarr;</a></li>";
                        			$('.pagination').append(li);
                        		}
                        		
                        		if((number+1) == totalPages) {
                        			var li = "<li class='disabled'><a href='#' page='"+totalPages+"'>&raquo;</a></li>";
                        			$('.pagination').append(li);
                        		} else {
                        			var li = "<li><a href='#' page='"+totalPages+"'>&raquo;</a></li>";
                        			$('.pagination').append(li);
                        		}
                    		}
                    		// end start pagination
                    		
                    	    //$(".pagination li.active a").css({"background-color":"#4CAF50", "color":"white"});
                    	    
                    	    var to = (number + 1) * size;
            		        if (to > totalElements) {
            		            to = totalElements;
            		        }
            		        
            		        var totalRow = obj.totalRow;
					        
						    if(search !== null && search !== '') {
					        	if(totalElements == 0) {
					        		$('#messagetable').text("Showing " + (0) + " to " + (to) + " of " + totalElements + " entries. (filtered from " + totalRow + " total entries)");
					        	} else {
					        		$('#messagetable').text("Showing " + ((number * size) + 1) + " to " + (to) + " of " + totalElements + " entries. (filtered from " + totalRow + " total entries)");
					        	}
					        	
					        } else {
					        	if(totalElements == 0) {
					        		$('#messagetable').text("Showing " + 0 + " to " + (to) + " of " + totalElements + " entries.");
					        	} else {
					        		$('#messagetable').text("Showing " + ((number * size) + 1) + " to " + (to) + " of " + totalElements + " entries.");
					        	}
					        	
					        }
            		        
						    if(obj.data.content.length == 0) {
						    	 var customerRow = "<tr class='no-data'><td colspan='5'><p>No data</p></td></tr>";
						    	 $('#table-data-server tbody').append(customerRow);
						    }
						    
            	            $.each(obj.data.content, function (i, o) {
            	            	var action =  '<td class="text-nowrap"> '
            	            		+'<a class="view" style="cursor: pointer;" data-toggle="modal" data-target=".myModal2" id="' + o.id + '"> <i class="fa fa-info text-inverse m-r-10" data-toggle="tooltip" data-id="' + o.id + '" data-placement="top" title="View" data-original-title="Tooltip top"></i></a>'
            	            		+'<a href="/cuti/edit/'+o.id+'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>'
            	            		+'<a href="#" data-toggle="tooltip" data-original-title="Close" class="delete" idx="'+o.id+'"> <i class="fa fa-close text-danger"></i> </a></td>';
            	            	var createdDate = '<span class="text-muted"><i class="fa fa-clock-o"></i> '+((o.createdDate == null) ? "" : o.formatCreatedDate)+'</span>';
            	            	
            	            	var customerRow = '<tr>'
            	            		+ '<td>'
            	                    + ((number * size) + (i + 1))
            	                    + '</td>'
                                    + '<td>'
                                    + o.jumlahCutiTahunan
                                    + '</td>'
                                    + '<td>'
                                    + o.type
                                    + '</td>'
                                    + '<td>'
                                    + createdDate
                                    + '</td>'
                                    + '<td>'
                                    + action
                                    + '</td>'
                                    + '</tr>';
                                $('#table-data-server tbody').append(customerRow);
            	        	})
            	        	$('.box').jmspinner(false);
            	        	info();
            	        	actionPagination();
            	            
            	        	 $( "a.delete" ).click(function() {
             	        		var idx = $(this).attr("idx");
             	        		console.log(idx)
             	        		swal("Yakin akan di hapus?", {
             	        			  buttons: {
             	        			    cancel: "Batal",
             	        			    catch: {
             	        			      text: "Delete",
             	        			      value: "catch",
             	        			    },
             	        			  },
             	        			})
             	        			.then((value) => {
             	        			  switch (value) {
             	        			 
             	        			    case "catch":
         	    	        			    $.ajax({
         	                           			  methode: "GET",
         	                           			  url: "/rest/cuti/delete/" + idx,
         	                           			  success: function() {
         	                           				  
         	                           				 $("#table-data-server tbody")
         	                                           .each(function () {
         	                                               $(this).children().remove();
         	                                           });
         	                               			$("#table-data-server tbody").fadeOut("slow");
         	                               			
         	                           				 pagination();
         	                           			  },
         	                           			  statusCode: {
         	  	                       				  400: function() {
         	  	                       					 swal(
         	  	                                    	           'Can not be deleted!',
         	  	                                    	           'Sorry, this data cannot be deleted. This data has relations with other data.',
         	  	                                    	           'warning'
         	  	                                    	     )
         	  	                       				  },
         	  	                       				  200: function() {
         	  	                       					 swal(
         	  	                                      	           'Deleted!',
         	  	                                      	           'Your file has been deleted.',
         	  	                                      	           'success'
         	  	                                      	     )
         	  	                       				  }
         	  	                       			  }
         	                           		  })
         	                           		  swal("Berhasil!", "Data berhasil dihapus!", "success");
             	        			      break;
             	        			 
             	        			  }
             	        			});
                          	}); 
                    	}
                    })
            		
            	}
        	})
        	
        	request.done(function( msg ) {
        		$('.box').jmspinner(false);
        	});
        		 
        	request.fail(function( jqXHR, textStatus ) {
        		$('.box').jmspinner(false);
        		$("#table-data-server tbody")
                .each(function (i) {
                     $(this).children().remove();
                });
        		var customerRow = "<tr class='no-data'><td colspan='6'><p>No data</p></td></tr>";
		    	 $('#table-data-server tbody').append(customerRow);
        	});
        	
    	}
    	
    }
    // end pagination()
    
});